 
# importing os module 
import os

def delete_file(directory, file_name, extension):
  '''This function deletes the file if exists \n
      Arguments:
        directory : string
        file_name : string
        extension : string
      Returns:
        remove the file if path exists \n
        return string (file does not exist) if file doesn't exist
        return string (invalid path) if path doesn't exist'''
  isExist = os.path.exists(directory)
  if isExist == True:
    file = file_name+extension
    dir_list = os.listdir(directory)
    # to check if a file exists or not
    if file_name+extension not in dir_list:
      return("file does not exist")
    # Remove the file
    else:
      path = os.path.join(location)
      os.remove(path)
      return("%s has been removed successfully" % file)
  else:
    return "invalid path"

#Driver program
path = r"C:\Users\rgollapudi\Documents\python"
folder_name = input("Enter the directory name")
directory = path+'\\'+folder_name
file_name = input("enter the file_name : ")
extension = input("enter the extension : ")
location = directory+'\\'+file_name+extension
print(delete_file(directory, file_name, extension))





